//
//  ContentView.swift
//  CrowTraceSwiftUI
//
//  Created by Saba on 31/08/20.
//  Copyright © 2020 Saba. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State private var CountryCode = ""
    @State private var PhoneNumber = ""
    @State private var isActive : Bool = false
    var body: some View {
        NavigationView{
            
            NavigationLink(destination: OTPview(), isActive: self.$isActive)
            {
                Text("")
            }
            
        VStack(alignment: .leading, spacing: 20) {
            Image("appicon2").resizable().frame(width: 100, height: 100, alignment: .topLeading).padding()
            Spacer()
            
            Text("Sign In")
                .foregroundColor(Color(red: 72/255, green: 73/255, blue: 161/255)).font(.largeTitle).bold()
            Text("to continue")
                .foregroundColor(Color(red: 72/255, green: 73/255, blue: 161/255)).font(.title)
            Spacer()
            
            HStack{
                CustomTextField(placeHolder: "+1", value: $CountryCode, lineColor: .gray, width: 2).padding([.leading , .trailing] , 0)
//                Image(systemName: "down-arrow-2")
                HStack{
                    CustomTextField(placeHolder: "PhoneNumber", value: $PhoneNumber, lineColor: .gray, width: 2).padding([.leading , .trailing] , 0)
                }
                Spacer()
            }
            Button(action:{ print("signin")
                self.isActive = true
            }) { Text("Sign In").font(.body).font(.custom("DM Sans", size: 10)).bold().foregroundColor(.white).frame(width: 100, height: 50).padding(.all , 10).padding([.leading , .trailing] , 25).background(Color(red: 72/255, green: 73/255, blue: 161/255)).cornerRadius(20)
                           }
        Spacer()
            
        } .padding(.all , 40)
    }.navigationViewStyle(StackNavigationViewStyle())
    // New Code
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


struct CustomTextField: View {
    var placeHolder: String
    @Binding var value: String

    var lineColor: Color
    var width: CGFloat

    var body: some View {
        VStack {
            TextField(self.placeHolder, text: $value)
            .padding()
                .font(.system(size: 20))
               .font(.custom("DM Sans", size: 20))

            Rectangle().frame(height: self.width)
                .padding(.horizontal, 20).foregroundColor(self.lineColor)
        }
    }
}
